# xilinx-xvc

Copy of the Xilinx XVC (distributed with Vivado) Xilinx Virtual Cable. This bundles a driver and a server so ILA cores in the FELIX FPGA can be accessed over PCIe, without the need of a JTAG cable.

The original files originate from the Vivado 2021.2 installation (Xilinx/Vivado/2021.2/data/xicom/drivers/pcie/xvc_pcie.zip), the original import is unchanged, changes made to dedicate the driver to FELIX can be found in the git log

# Add another BINARY_TAG

just run "make" in the xvcserver directory
